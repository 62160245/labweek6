package oxframe06;

import java.io.Serializable;

public class Player implements Serializable{
    private char name;
    private int win;
    private int lose;
    private int draw;
    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }
    
    public void win() {
        win++;
    }
    
    public void lose() {
        lose++;
    }
    
    public void draw() {
        draw++;
    }
    
    
    public int getLose() {
        return lose;
    }


    public int getDraw() {
        return draw;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
    
}
