package oxframe06;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TestReadobject {
    public static void main(String[] args){
        File file = null; 
        FileInputStream fis = null;
        ObjectInputStream ois = null ;
        Player x = null;
        Player o = null;
        try {
            file = new File("ox.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            x = (Player)ois.readObject();
            o = (Player)ois.readObject();
            System.out.println(x);
            System.out.println(o);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestReadobject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestReadobject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadobject.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
